package com.company;

import java.util.Arrays;

public class AList implements IList{

    private final int DEFAULT_CAPACITY=10;
    private int index;
    private int[] array;
    private int size;

    public AList(){
        this.array=new int[DEFAULT_CAPACITY];
    }

    public AList(int capacity){
        this.array = new int[capacity];
        if (capacity < DEFAULT_CAPACITY) {
            this.array = new int[DEFAULT_CAPACITY];
        }

    }
    public AList(int [] array){
        this();
        for (int i = 0; i < array.length; i++) {
            add(array[i]);
        }
    }



    @Override
    public void clear() {
        for (int i=0; i<this.array.length; i++){
            this.array[i]= 0;
        }
        size=0;
    }

    @Override
    public int size() {

        return size;
    }
    public int length() {

        return array.length;
    }

    @Override
    public int get(int index) {

        return array[index];
    }

    @Override
    public boolean add(int value) {
        if(index==array.length){
            ensureCapacity();
        }
        array[index]=value;
        size++;
        index++;
        return true;
    }

    private void ensureCapacity() {
        int[] newArray = new int[(int) (array.length*1.5)];
        System.arraycopy(array, 0, newArray, 0,length());
        array = newArray;

    }

    @Override
    public boolean add(int index, int value) {

        int[] arr = new int[array.length + 1];
        if (index < 0) {
            return false;
        }
        if (index > size) {
            ensureCapacity();
            array[index] = value;
            size++;
            return true;
        }
        if (index == array.length) {
            ensureCapacity();
            array[index] = value;
            size++;
            return true;
        }
        if (array[index] != 0 && index < array.length && size < array.length - 1) {
            for (int i = 0; i < array.length; i++) {
                arr[i + 1] = array[i];
                array[i] = arr[i];
            }
            if (index < array.length && size < array.length - 1) {
                array[index] = value;
                int count = 0;
                for (int i = 0; i < array.length; i++) {
                    if (array[i] != 0) {
                        count++;
                    }
                }
                size = count;

            }
        }return true;
    }

    @Override
    public void remove(int value) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                array[i] = array[i + 1];
            }
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                count++;
            }
        }
        size = count;
    }

    @Override
    public void removeByIndex(int index) {

        int count=0;
        for (int i = index; i<size; i++)
            array[i] = array[i+1];
        array[size] = 0;
        for(int i=0; i<array.length; i++){
            if(array[i]!=0){
                count++;
            }
        }
        size=count;
    }

    @Override
    public boolean contains(int value) {

        for (int i = 0; i < this.array.length; i++) {
            for (int j = 0; j < this.array.length; j++) {
                if (this.array[j] == (value)) {
                    return true;

                }
            }

        }
        return false;
    }

    @Override
    public boolean set(int index, int value) {

        int count=0;
        if (index>0 && index<array.length){
            this.array[index]=value;
            return true;
        }else {
            System.out.println("IndexOutOfBoundsException");
        }
        return true;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(this.array));
        }


    @Override
    public int[] toArray() {
        return new int[0];
    }

    @Override
    public boolean removeAll(int[] ar) {
        int count=0;
        for (int index=0; index<this.array.length; index++){
            for (int i=0; i<ar.length; i++) {
                if (this.array[index] == ar[i]) {
                    this.array[index]= 0;
                }
            }
        }
        for(int i=0; i<this.array.length; i++){
            if(this.array[i]!=0){
                count++;
            }
            size=count;
        }
        return true;
    }


}
